import styled from 'styled-components';
import theme from '../../theme/theme';
import { touch } from 'react-native';
const styles = {};

styles.Container = styled.View`
  flex: 1;
  margin-top: ${theme.SH * 0.25}
  margin-bottom: ${theme.SH * 0.2}
	justify-content: space-around;
	align-items: center;
`;

styles.Title = styled.Text`
	text-align: center;
	font-family: AvenirNext-Medium;
	font-size: 32px;
	line-height: 38px;
	text-align: right;
	color: #43518e;
`;

styles.Content = styled.Text`
	font-size: 14px;
	line-height: 21px;
	text-align: center;
	color: #515258;
	width: ${theme.SW * 0.685};
`;

styles.ButtonContainer = styled.View`
	width: 100%;
	flex-direction: row;
	justify-content: space-around;
`;

styles.Button = styled.TouchableOpacity`
	width: ${theme.SW * 0.4};
	height: ${theme.SH * 0.078};
	background-color: ${props => props.backgroundColor};
	justify-content: center;
	border-radius: 4px;
	border: 1px solid #607bee;
`;

styles.Text = styled.Text`
	font-size: 14px;
	line-height: 21px;
	text-align: center;
	color: ${props => props.color};
`;

export default styles;
