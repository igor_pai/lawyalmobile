import React from 'react';
import Logo from '../../assets/images/logo.svg';
import styles from './styles';
import theme from '../../theme/theme';

const Welcome = props => {
	const _startOnBoarding = () => {
		props.navigation.navigate('OnBoarding');
	};
	const _goToLogin = () => {
		props.navigation.replace('WebSite');
	};
	return (
		<>
			<styles.Container>
				<Logo width={theme.SH * 0.58} />
				<styles.Title>ברוכים הבאים</styles.Title>
				<styles.Content>
					אנו נעזור לכם לנהל ולעקוב אחר דיווחי השעות מהכיס ביעילות ומהירות.
					דפדפו וגלו מה אפשר לעשות באפליקציה.
				</styles.Content>
				<styles.ButtonContainer>
					<styles.Button
						backgroundColor={theme.whiteColor}
						onPress={_goToLogin}
					>
						<styles.Text color={theme.blueColor}>התחברות</styles.Text>
					</styles.Button>
					<styles.Button
						backgroundColor={theme.blueColor}
						onPress={_startOnBoarding}
					>
						<styles.Text color={theme.whiteColor}>ספרו לי עוד</styles.Text>
					</styles.Button>
				</styles.ButtonContainer>
			</styles.Container>
		</>
	);
};

export default Welcome;
