import styled from 'styled-components';
import theme from '../../theme/theme';

const styles = {};

styles.IconContainer = styled.View`
	position: absolute;
	top: ${props => props.top}px;
	left: ${props => props.left}px;
`;
export default styles;
