import React, { useRef, useState, useEffect } from 'react';
import { View, Image } from 'react-native';
import SlideIcon from './SlideIcon';
import theme from '../../theme/theme';
import asyncStorage from '../../utilis/storageService';
const icons = [
	{ iconName: 'icon_2', top: theme.SH * 0.34, left: theme.SW * -0.09 },
	{ iconName: 'icon_3', top: theme.SH * 0.29, left: theme.SW * 0.17 },
];

const SplashScreen = props => {
	const [iconData, setIconData] = useState({
		iconName: 'icon_1',
		top: theme.SH * 0.4,
		left: theme.SW * -0.248,
	});
	const intervalId = useRef(null);
	const index = useRef(0);
	const firstRun = useRef(null);

	useEffect(() => {
		intervalId.current = setInterval(() => {
			if (index.current > 1) {
				done();
			} else {
				setIconData(icons[index.current]);
				index.current = index.current + 1;
			}
		}, 500);
		checkIsFirstRun();
	}, []);

	const done = async () => {
		clearInterval(intervalId.current);

		if (firstRun.current !== null) {
			props.navigation.replace('WebSite');
		} else {
			props.navigation.replace('Welcome');
			asyncStorage.setItem('isFirstRun', 'true');
		}
	};

	const checkIsFirstRun = async () => {
		firstRun.current = await asyncStorage.getItem('isFirstRun');
	};

	return (
		<>
			<View
				style={{
					flex: 1,
					backgroundColor: '#607BEE',
					justifyContent: 'center',
				}}
			>
				<SlideIcon
					iconName={iconData.iconName}
					top={iconData.top}
					left={iconData.left}
				/>
			</View>
		</>
	);
};

export default SplashScreen;
