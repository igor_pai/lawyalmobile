import React from 'react';
import { StyleSheet } from 'react-native';
import { splashScreen } from '../../assets/images/index';
import styles from './styles';

const SlideIcon = ({ iconName, top, left }) => {
	const Icon = splashScreen[iconName];
	return (
		<styles.IconContainer top={top} left={left}>
			<Icon />
		</styles.IconContainer>
	);
};

export default SlideIcon;
