import React, { useEffect, useRef, useState } from 'react';
import { BackHandler } from 'react-native';
import { WebView } from 'react-native-webview';
import asyncStorage from '../../utilis/storageService';
import url from 'url';

const WebSite = props => {
	const webview = useRef(null);
	const [canGoBack, setCanGoBack] = useState(false);
	const onAndroidBackPress = () => {
		console.log('canGoBack', canGoBack);
		console.log(webview.current);
		if (webview.current) {
			webview.current.goBack();
			return true; // prevent default behavior (exit app)
		}
		return false;
	};
	useEffect(() => {
		BackHandler.addEventListener('hardwareBackPress', onAndroidBackPress);
		return () => {
			BackHandler.removeEventListener('hardwareBackPress', onAndroidBackPress);
		};
	}, []);

	const onNavigationStateChange = navState => {
		setCanGoBack(navState.canGoBack);
	};

	const onMessage = event => {
		console.log('data', event.nativeEvent.data);
		const { data } = event.nativeEvent;

		if (event.nativeEvent.data) asyncStorage.setItem('token', data);
		//console.log();

		// console.log('data', data);

		// if (data.includes('Cookie:')) {
		// 	console.log('in Cookie');
		// 	// process the cookies
		// }
	};
	//window.ReactNativeWebView.postMessage(JSON.stringify(window.location));
	//localStorage.setItem('token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6Im5ldGFuZWxAaGVyb2xvLmNvLmlsIiwiaWF0IjoxNTg3NDkzOTI1LCJleHAiOjE1ODc1ODAzMjUsInVzZXJfaWQiOjExLCJvcmlnX2lhdCI6MTU4NzQ5MzkyNX0.WXKYUi5XgY-eggVdzeH2x0VCoYCJuJTM90H2xX0cBe0');
	const INJECTED_JAVASCRIPT = `(function() {
		localStorage.removeItem('token');
})();`;
	return (
		<>
			<WebView
				source={{
					uri:
						'http://lawyal-dev-frontend.s3-website-us-east-1.amazonaws.com/reports',
				}}
				cacheEnabled={true}
				cacheMode='LOAD_CACHE_ELSE_NETWORK'
				ref={webview}
				onNavigationStateChange={onNavigationStateChange}
				onMessage={onMessage}
				javaScriptEnabled={true}
				mixedContentMode='compatibility'
				sharedCookiesEnabled={true}
				thirdPartyCookiesEnabled={true}
			/>
		</>
	);
};

export default WebSite;
