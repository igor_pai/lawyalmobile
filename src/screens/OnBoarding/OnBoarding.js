import React, { useRef } from 'react';
import { View, Image } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import styles from './styles';
import OnboardingIcon from './OnboardingIcon';

const slides = [
	{
		key: 0,
		title: 'דיווחים',
		text: `נגמרו התירוצים :) הוספת דיווחי שעות והוצאות בכמה קליקים מכל מקום ובכל זמן`,
		btnText: 'המשך',
		image: 'image_1',
	},
	{
		key: 1,
		title: `נשארים בעניינים`,
		text: `זה לא תמיד פשוט… אחנו נעזור לך להבין את התמונה המלאה ע״י סיכומים נוחים ופשוטים`,
		backgroundColor: '#febe29',
		btnText: 'בוא נתחיל',
		image: 'image_2',
	},
	{
		key: 2,
		title: `תנו לנו להזכיר לכם`,
		text: `לפעמים שוכחים… האפליקציה תזכיר לכם לקראת סוף היום לדווח את השעות שלכם. רק צריך לאשר לנו במסך הבא`,
		backgroundColor: '#febe29',
		btnText: 'בוא נתחיל',
		image: 'image_3',
	},
];

const OnBoarding = props => {
	const appIntroSlider = useRef(null);
	const nextPage = index => {
		if (index < 3) appIntroSlider.current.goToSlide(index);
		else skip();
	};
	const skip = () => {
		props.navigation.replace('WebSite');
	};
	_renderItem = ({ item }) => {
		return (
			<styles.mainContentContainer>
				<styles.SkipButtonContinaer>
					<styles.SkipButton onPress={skip}>
						<styles.SkipButtonText>התחברות</styles.SkipButtonText>
					</styles.SkipButton>
				</styles.SkipButtonContinaer>
				<styles.ImageContinaer>
					<OnboardingIcon iconName={item.image} />
				</styles.ImageContinaer>
				<View style={{ justifyContent: 'center', alignItems: 'center' }}>
					<styles.TextTitle>{item.title}</styles.TextTitle>
					<styles.ContentText>{item.text}</styles.ContentText>
				</View>
				<styles.Button onPress={() => nextPage(item.key + 1)}>
					<styles.Text>{item.btnText}</styles.Text>
				</styles.Button>
			</styles.mainContentContainer>
		);
	};
	return (
		<>
			<AppIntroSlider
				renderItem={_renderItem}
				data={slides}
				showNextButton={false}
				showDoneButton={false}
				showSkipButton={false}
				dotStyle={{ backgroundColor: '#E2E9FF' }}
				activeDotStyle={{ backgroundColor: '#607BEE' }}
				ref={appIntroSlider}
			/>
		</>
	);
};

export default OnBoarding;
