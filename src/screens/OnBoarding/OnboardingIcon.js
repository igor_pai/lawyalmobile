import React from 'react';
import { onBoarding } from '../../assets/images/index';

const OnboardingIcon = ({ iconName }) => {
	const Icon = onBoarding[iconName];
	return <Icon />;
};

export default OnboardingIcon;
