import styled from 'styled-components';
import theme from '../../theme/theme';

const styles = {};

styles.mainContentContainer = styled.View`
	height: 90%;
	justify-content: space-around;
	align-items: center;
`;

styles.SlideImage = styled.Image`
	width: ${props => props.theme.SW};
	height: ${props => props.theme.SH * 0.4};
	position: absolute;
	top: 150;
`;

styles.TextContainer = styled.View`
	align-items: center;
	width: 100%;
	position: absolute;
	bottom: 0;
	height: 40%;
	justify-content: space-around;
`;

styles.TextTitle = styled.Text`
	text-align: center;
	font-family: AvenirNext-Medium;
	font-size: 32px;
	line-height: 38px;
	text-align: right;
	color: #43518e;
`;

styles.Text = styled.Text`
	text-align: center;
	line-height: 20px;
	color: white;
`;

styles.ButtonContainer = styled.View`
	height: 50px;
	width: 330px;
	border-radius: 4px;
	background-color: #607bee;
	box-shadow: 0 3px 6px rgba(56, 51, 93, 0.31);
`;

styles.Button = styled.TouchableOpacity`
	height: 50px;
	width: 330px;
	justify-content: center;
	border-radius: 4px;
	background-color: #607bee;
`;

styles.SkipButtonContinaer = styled.View`
	display: flex;
	justify-content: flex-start;
	width: 90%;
`;
styles.SkipButton = styled.TouchableOpacity`
	align-self: flex-start;
`;
styles.SkipButtonText = styled.Text``;

styles.ContentText = styled.Text`
	font-size: 14px;
	line-height: 21px;
	text-align: center;
	color: #515258;
	width: ${theme.SW * 0.685};
`;

styles.ImageContinaer = styled.View``;

export default styles;
