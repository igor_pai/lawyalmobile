import image_1 from './onBoarding/image_1.svg';
import image_2 from './onBoarding/image_2.svg';
import image_3 from './onBoarding/image_3.svg';

import icon_1 from './splashScreen/icon_1.svg';
import icon_2 from './splashScreen/icon_2.svg';
import icon_3 from './splashScreen/icon_3.svg';

export const onBoarding = {
	image_1,
	image_2,
	image_3,
};

export const splashScreen = {
	icon_1,
	icon_2,
	icon_3,
};
