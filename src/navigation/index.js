import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
// components
import OnBoarding from '../screens/OnBoarding/OnBoarding';
import SplashScreen from '../screens/SplashScreen/SplashScreen';
import WebSite from '../screens/WebSite/WebSite';
import Welcome from '../screens/Welcome/Welcome';

const Stack = createStackNavigator();

const MainStack = props => {
	return (
		<NavigationContainer>
			<Stack.Navigator
				screenOptions={{
					headerShown: false,
				}}
			>
				<Stack.Screen name='SplashScreen' component={SplashScreen} />
				<Stack.Screen name='Welcome' component={Welcome} />
				<Stack.Screen name='OnBoarding' component={OnBoarding} />
				<Stack.Screen name='WebSite' component={WebSite} />
			</Stack.Navigator>
		</NavigationContainer>
	);
};

export default MainStack;
