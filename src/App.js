/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useRef } from 'react';
import {
	SafeAreaView,
	StyleSheet,
	View,
	StatusBar,
	I18nManager,
} from 'react-native';
import MainStack from './navigation/index';

I18nManager.forceRTL(false);
I18nManager.allowRTL(false);

const App = () => {
	return (
		<>
			<StatusBar />
			<SafeAreaView style={{ flexGrow: 1 }}>
				<View style={{ flex: 1 }}>
					<MainStack />
				</View>
			</SafeAreaView>
		</>
	);
};

const styles = StyleSheet.create({});

export default App;
