import { Dimensions } from 'react-native';

const SW = Dimensions.get('screen').width;
const SH = Dimensions.get('screen').height;

export default theme = {
	SW,
	SH,
	blueColor: '#607bee',
	whiteColor: '#FFFFFF',
};
